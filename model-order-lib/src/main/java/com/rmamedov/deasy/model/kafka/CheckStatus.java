package com.rmamedov.deasy.model.kafka;

public enum CheckStatus {
    ADDRESSES_CHECKED,
    ORDER_MENU_CHECKED,
    COURIER_CHECKED,
    FULLY_CHECKED
}
