package com.rmamedov.restorauntservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableDiscoveryClient
@SpringBootApplication
public class RestorauntServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestorauntServiceApplication.class, args);
    }

}
