config-service: 8888
discovery-service: 8761
zuul-service: 8762

order-service: 8000 - 8019
address-service: 8020 - 8039
restoraunt-service: 8040 - 8079
courier-service: 8080 - 8099
payment-service: 8100 - 8119