package com.rmamedov.deasy.model.kafka;

import lombok.Data;

@Data
public class Address {

    private String address;

}
